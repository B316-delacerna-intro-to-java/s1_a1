package com.zuitt.activity1;
import java.util.Scanner;

public class Activity1 {
    public static void main(String[] args) {

        //First Name
        Scanner fNameObj = new Scanner(System.in);
        System.out.println("First Name:");

        String firstName = fNameObj.nextLine(); //Read user input

        //Last Name
        Scanner lNameObj = new Scanner(System.in);
        System.out.println("Last Name:");

        String lastName = lNameObj.nextLine(); //Read user input

        //First Subject Grade
        Scanner firstSubjGradeObj = new Scanner(System.in);
        System.out.println("First Subject Grade:");

        double firstGrade = new Double(firstSubjGradeObj.nextLine()); //Read user input

        //Second Subject Grade
        Scanner secondSubjGradeObj = new Scanner(System.in);
        System.out.println("Second Subject Grade:");

        double secondGrade = new Double(secondSubjGradeObj.nextLine());

        //Third Subject Grade
        Scanner thirdSubjGradeObj = new Scanner(System.in);
        System.out.println("Third Subject Grade:");

        double thirdGrade = new Double(thirdSubjGradeObj.nextLine());

        double averageGrade = new Double((firstGrade + secondGrade + thirdGrade)/3);

        System.out.println("Good day, " + firstName +" "+ lastName+".");
        System.out.println("Your grade average is: " + averageGrade);
    }
}
