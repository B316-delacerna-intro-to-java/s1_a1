package com.zuitt.example;

public class Variables {
    public static void main(String[] args) {
        //Variable Declaration
        int age;
        char middle_name;

        //Variable declaration vs initialization
        int x;
        int y = 0;

        y = y + 2;

        //Print out into the console "The value of y is " + (value)
        System.out.println("The value of y is " + y);

    }
}
